Inneholder installasjonsfiler for MRBS-løsning som er installert for Utstillingsplassen til følgende lokasjoner:
- Kjonerud 
- Vangsvegen 73
- Parkgata 36

Enkelte endringer som skal gjøres gjelder for alle instanser, og det er laget et script for å holde instansene i sync.
Dvs:
Endringer som gjelder styling, bilder og språkfiler skal reflekteres på alle instansene.
- Gjør endringer i css, theme, images og locale på "kj1" instansen, og kjør deretter 'instance-clone.sh', som vil kopiere over aktuelle filer til de andre instansene.

Se for øvrig Confluence: https://headit.atlassian.net/wiki/display/kunder/Utstillingsplassen
Se for øvrig beskrivelse i 'instance-clone.sh'
