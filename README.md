# README #

### What is this repository for? ###
MRBS (møteromsbooking) som administreres av Headit.
For detaljer relatert til kundespesifikke instanser, se underkatalogene i dette prosjektet for detaljert beskrivelse.

### RELEASE ###
Ved deploy av ny versjon, lag en ny release-branch, oppdater "version"-fila med rett versjonsnummer.
Avslutt release-branch og oppdater version-fil med bumped versjonsnummer og legg til snapshot på develop-branchen.
Legg ut filer fra release-branch til prod.

### Installasjonsdetaljer ###
* https://headit.atlassian.net/wiki/display/kunder/MRBS+Installasjonsdetaljer

### Who do I talk to? ###
* Development: Cato Mausethagen
* Project details: Morten Galaassen


### Version history ###
- 2021-12-14: Added new room for: Parkgata 36
  - Jira: https://headit.atlassian.net/browse/UPL-1
  - Added the room using the GUI for Parkgata installation
    - http://booking.upl.no/pg36/
    - Username and password stored in LastPass
- 2020-08-05: Added new location: Parkgata 36
    - Copied `vv73` folder to `pg36` folder
    - Created `kj1.tables.my.sql` database script to `pg36.tables.my.sql` (`./utstillingsplassen/database`)
    - Adjusted database prefix to `pg36` for the new file
    - Adjusting config in `utstillingsplassen/pg36/config.inc.php` to match the new `pg36` instance (remember the database-prefix `pg36_mrbs`)
