#!/bin/bash 

# Clone-script for oppdatering av VV73-instans, dersom det gjøres endringer i styling eller språkfiler
## 
## Benyttes ved endringer av:
##  - style (kj1/Themes/*, kj1/css/custom.css, kj1/images/upl_logo.png)
##  - spraakfiler (kj1/lang/lang.no)
## 
## MERK! config.inc.php er mer eller mindre identisk for de to instansene, men med forskjellig prefix på databasetabellene.
## Dersom det skal gjøres endringer i config.inc.php må denne oppdateres manuelt på begge instansene!
##
## Endringer for ovennevnte filer skal alltid gjøres på Kjonerud-instansen (kj1).
## Når endringer er testet ok: kjør denne script-filen for å kopiere nødvendige endringer over til Vangsvegen 73 og Parkgata 36-instansene.

SOURCE_ROOT="kj1"
SOURCE_CSS="$SOURCE_ROOT/css"
SOURCE_THEMES="$SOURCE_ROOT/Themes/utstillingsplassen"
SOURCE_IMAGES="$SOURCE_ROOT/images"
SOURCE_LANG="$SOURCE_ROOT/lang"

declare -a TARGET_ROOT=("vv73" "pg36")
TARGET_CSS="css"
TARGET_THEMES="Themes/utstillingsplassen"
TARGET_IMAGES="images"
TARGET_LANG="lang"

run() {
   echo # new line
   echo "Husk å stå i katalogen som scriptet ligger før det kjøres!"
   read -p "Kopiere innhold fra '$SOURCE_ROOT/' til '${TARGET_ROOT[*]}' (y | Y to continue, any other key to abort)? " -n 1 -r
   echo # new line
   if [[ $REPLY =~ ^[Yy]$ ]] ; then
      cp_css
      cp_themes
      cp_images
      cp_lang
      echo "All done!"
   fi

   # Kopiere php-filene fra Kjonerud til Vangsvegen og Parkgata - OBS! Vær sikker på at du ønsker dette (Kjonerud har spesialtilpassede tabeller)
   echo #
   echo #
   read -p "OBS! Kopiere PHP-filer fra '$SOURCE_ROOT/' til '${TARGET_ROOT[*]}' ('yes' to continue, ctrl+c to abort)? "
   echo # new line
   if [[ $REPLY == 'yes' ]] ; then
      read -p "Are you really sure ('y' to continue)?"
      if [[ $REPLY == 'y' ]] ; then
         cp_php_files
         echo "All done!"
      fi
   fi
   echo #

}

cp_php_files() {
   echo "   Copy php files from '$SOURCE_ROOT' to '${TARGET_ROOT[*]}'"
   echo "      day.php"
   echo "      month.php"
   echo "      report.php"
   echo "      search.php"
   echo "      week.php"
   for target in "${TARGET_ROOT[@]}"
   do
     echo "   cp $SOURCE_ROOT/day.php $target/day.php"
     cp $SOURCE_ROOT/day.php $target/day.php
     echo "   cp $SOURCE_ROOT/day.php $target/month.php"
     cp $SOURCE_ROOT/month.php $target/month.php
     echo "   cp $SOURCE_ROOT/day.php $target/report.php"
     cp $SOURCE_ROOT/report.php $target/report.php
     echo "   cp $SOURCE_ROOT/day.php $target/search.php"
     cp $SOURCE_ROOT/search.php $target/search.php
     echo "   cp $SOURCE_ROOT/day.php $target/week.php"
     cp $SOURCE_ROOT/week.php $target/week.php
   done
   echo "   Completed!"
}

cp_css() {
   echo "   Copy css files from '$SOURCE_CSS' to '${TARGET_ROOT[*]} -> /$TARGET_CSS'"
   for target in "${TARGET_ROOT[@]}"
   do
     echo "   cp $SOURCE_CSS/custom.css $target/$TARGET_CSS/custom.css"
     cp $SOURCE_CSS/custom.css $target/$TARGET_CSS/custom.css
   done
   echo "     Completed!"
}

cp_themes() {
   echo "   Copy theme files from '$SOURCE_THEMES' to '${TARGET_ROOT[*]} -> /$TARGET_THEMES'"
   for target in "${TARGET_ROOT[@]}"
   do
     echo "   cp $SOURCE_THEMES/styling.inc $target/$TARGET_THEMES/styling.inc"
     cp $SOURCE_THEMES/styling.inc $target/$TARGET_THEMES/styling.inc
   done
   echo "     Completed!"

}

cp_images() {
   echo "   Copy image files from '$SOURCE_IMAGES' to '${TARGET_ROOT[*]} -> /$TARGET_IMAGES'"
   for target in "${TARGET_ROOT[@]}"
   do
     echo "   cp -R $SOURCE_IMAGES/. $target/$TARGET_IMAGES/"
     cp -R $SOURCE_IMAGES/. $target/$TARGET_IMAGES/
   done
   echo "     Completed!"
}

cp_lang() {
   echo "   Copy language files from '$SOURCE_LANG' to '${TARGET_ROOT[*]} -> /$TARGET_LANG'"
   for target in "${TARGET_ROOT[@]}"
   do
     echo "   cp -R $SOURCE_LANG/. $target/$TARGET_LANG/"
     cp -R $SOURCE_LANG/. $target/$TARGET_LANG/
   done
   echo "     Completed!"
}

run
